import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { randomUUID } from 'crypto';
import { LoggerModule } from 'nestjs-pino';
import { AsyncLocalStorageModule } from '../asl/asl.module';
import { AsyncLocalStorageService } from '../asl/asl.service';

@Global()
@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DB_HOST,
      port: +process.env.DB_PORT,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_DATABASE,
    }),
    AsyncLocalStorageModule,
    LoggerModule.forRootAsync({
      imports: [AsyncLocalStorageModule],
      inject: [AsyncLocalStorageService],
      useFactory(aslService: AsyncLocalStorageService) {
        return {
          pinoHttp: {
            level: process.env.NODE_ENV !== 'production' ? 'debug' : 'info',
            transport:
              process.env.PRETTY_LOGS === 'true'
                ? { target: 'pino-pretty' }
                : undefined,
            redact: {
              paths: ['req.headers'],
              censor: '[**HIDDEN**]',
            },
            genReqId(req, res) {
              return randomUUID();
            },
          },
        };
      },
    }),
  ],
  exports: [ConfigModule, TypeOrmModule, AsyncLocalStorageModule, LoggerModule],
})
export class CommonModule {}

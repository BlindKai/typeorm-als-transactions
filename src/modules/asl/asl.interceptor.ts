import {
  CallHandler,
  ExecutionContext,
  Logger,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, tap } from 'rxjs';
import { AsyncLocalStorageService } from './asl.service';

/**
 * #### Summary:
 * This is a prototype of the solution for handling TypeORM SQL Transactions within the AsyncLocalStorage
 *
 * #### Motivation:
 * In order to use SQL transactions in different services or methods, it should be either injected to
 * the scope or passed explicitly. The last choice is obvious but sometimes quite tedious as it requires
 * to pass queryRunner to every single method which is not really preferable
 *
 * #### Additional ideas:
 * - [ ] Perhaps we should re-implement it so we could have many separated transactions within the request
 * - [x] Automatically close transaction if it wasn't closed and raise a WARN message
 *
 * #### References:
 *
 * There is an StackOverflow question related to this implementation:
 * https://stackoverflow.com/a/72448161/9871059
 *
 * There is also the change that will allow using ASL without struggling officially available in Nest.js 10.0.0:
 * https://github.com/nestjs/nest/pull/11142
 */
export class AslInterceptor implements NestInterceptor {
  private readonly logger = new Logger(AslInterceptor.name);

  constructor(private readonly aslService: AsyncLocalStorageService) {}

  intercept(
    context: ExecutionContext,
    next: CallHandler<any>,
  ): Observable<any> | Promise<Observable<any>> {
    this.logger.debug('Request');
    const requestContext = this.aslService.getBaseContext();

    return new Observable((subscriber) => {
      const subscription = this.aslService.run(requestContext, () => {
        return next
          .handle()
          .pipe(
            tap(async () => {
              this.logger.debug('Last resort check');
              await this.aslService.lastResortCheck();
              this.logger.debug('Last resort check completed');
            }),
          )
          .subscribe(subscriber);
      });

      return () => subscription.unsubscribe();
    });
  }
}

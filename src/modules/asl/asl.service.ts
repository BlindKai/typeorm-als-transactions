import { Injectable, Logger } from '@nestjs/common';
import { AsyncLocalStorage } from 'async_hooks';
import { randomUUID } from 'crypto';
import { DataSource } from 'typeorm';
import { AsyncContext } from './entities/AsyncContext';

@Injectable()
export class AsyncLocalStorageService extends AsyncLocalStorage<AsyncContext> {
  private readonly logger = new Logger(AsyncLocalStorageService.name);

  constructor(private readonly dataSource: DataSource) {
    super();
  }

  getBaseContext(): AsyncContext {
    this.logger.verbose('Getting base context');
    return { requestId: randomUUID() };
  }

  getTransaction() {
    this.logger.verbose('Get transaction');
    const queryRunner = this.getStore().queryRunner;

    if (!queryRunner || queryRunner.isReleased) {
      throw new Error('There is no active query runner within the context');
    }

    return queryRunner;
  }

  async createTransaction() {
    this.logger.verbose('Create transaction');
    const store = this.getStore();
    store.queryRunner = this.dataSource.createQueryRunner();
    await store.queryRunner.connect();

    return store.queryRunner;
  }

  async releaseTransaction() {
    this.logger.verbose('Release transaction');
    const store = this.getStore();

    await store.queryRunner.release();
  }

  async lastResortCheck() {
    this.logger.verbose('Last Resort check');
    const store = this.getStore();
    const queryRunner = store.queryRunner;

    if (!queryRunner) {
      this.logger.verbose('No unreleased query runner detected');
      return;
    }

    if (queryRunner.isTransactionActive) {
      this.logger.warn('Please, do not forget to commit your transaction');
      await queryRunner.rollbackTransaction();
      this.logger.warn('Transaction was rollback automatically');
    }

    if (!queryRunner.isReleased) {
      this.logger.warn('Please, do not forget to release database connection');
      await queryRunner.release();
      this.logger.warn('Transaction was released automatically');
    }
  }
}

import { Module } from '@nestjs/common';
import { AsyncLocalStorageService } from './asl.service';

@Module({
  providers: [AsyncLocalStorageService],
  exports: [AsyncLocalStorageService],
})
export class AsyncLocalStorageModule {}

import { QueryRunner } from 'typeorm';

export class AsyncContext {
  requestId: string;
  queryRunner?: QueryRunner;
}

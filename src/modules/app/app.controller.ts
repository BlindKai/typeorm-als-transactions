import { Controller, Get, Logger } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);

  constructor(private readonly appService: AppService) {}

  @Get('')
  runWithTransaction() {
    this.logger.log('Run with transaction');

    return this.appService.runWithTransaction();
  }

  @Get('forgot')
  forgotToCloseTransaction() {
    this.logger.log('Forgot to close transaction');

    return this.appService.forgotToCloseTransaction();
  }
}

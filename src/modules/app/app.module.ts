import { Logger, MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { AsyncLocalStorageService } from '../asl/asl.service';
import { CommonModule } from '../common/common.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Request, Response, NextFunction } from 'express';

@Module({
  imports: [CommonModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  private readonly logger = new Logger(AppModule.name);

  constructor(private readonly aslService: AsyncLocalStorageService) {}

  configure(consumer: MiddlewareConsumer) {
    // consumer
    //   .apply((req: Request, res: Response, next: NextFunction) => {
    //     this.logger.debug('Request');
    //     const context = this.aslService.getBaseContext();
    //     this.aslService.run(context, () => next());
    //   })
    //   .forRoutes('*');
  }
}

import { Injectable } from '@nestjs/common';
import { AsyncLocalStorageService } from '../asl/asl.service';

@Injectable()
export class AppService {
  constructor(private readonly aslService: AsyncLocalStorageService) {}

  async runWithTransaction() {
    const queryRunner = await this.aslService.createTransaction();

    const response = await queryRunner.query('SELECT now() AS now');

    const toReturn = 'Well done ' + response[0]?.now;

    await queryRunner.release();

    return toReturn;
  }

  async forgotToCloseTransaction() {
    const queryRunner = await this.aslService.createTransaction();

    const response = await queryRunner.query('SELECT now() AS now');

    const toReturn = 'Still good ' + response[0]?.now;

    // Let's say that we don't to the release await queryRunner.release();

    return toReturn;
  }
}

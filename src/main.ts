import { NestFactory } from '@nestjs/core';
import { Logger } from 'nestjs-pino';
import { AppModule } from './modules/app/app.module';
import { AslInterceptor } from './modules/asl/asl.interceptor';
import { AsyncLocalStorageService } from './modules/asl/asl.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { bufferLogs: true });
  app.useLogger(app.get(Logger));

  app.useGlobalInterceptors(
    new AslInterceptor(app.get(AsyncLocalStorageService)),
  );

  await app.listen(3000);
}
bootstrap();
